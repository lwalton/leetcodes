const stringy = list => {
  let mestring = "";
  while (list != null) {
    mestring += list.val.toString();
    list = list.next;
  }
  return mestring
    .split("")
    .reverse()
    .join("");
};
const addTwoNumbers = (l1, l2) => {
  let outval = (BigInt(stringy(l1)) + BigInt(stringy(l2))).toString();
  let output = null;
  [...outval].forEach(cha => {
    let newVal = new ListNode(cha);
    newVal.next = output;
    output = newVal;
  });
  return output;
};

console.log(
  addTwoNumbers(
    {
      val: 2,
      next: { val: 4, next: { val: 3, next: null } }
    },
    {
      val: 5,
      next: { val: 6, next: { val: 4, next: null } }
    }
  )
);

//MAP
const stringy = list => {
  let mestring = "";
  while (list != null) {
    mestring += list.val.toString();
    list = list.next;
  }
  return mestring
    .split("")
    .reverse()
    .join("");
};
const addTwoNumbers = (l1, l2) => {
  let outval = (BigInt(stringy(l1)) + BigInt(stringy(l2))).toString();
  let output = null;

  [...outval].map(cha => {
    let newVal = new ListNode(cha);
    newVal.next = output;
    output = newVal;
  });
  return output;
};

//For loop
const stringy = list => {
  let mestring = "";
  while (list != null) {
    mestring += list.val.toString();
    list = list.next;
  }
  return mestring
    .split("")
    .reverse()
    .join("");
};
const addTwoNumbers = (l1, l2) => {
  let outval = (BigInt(stringy(l1)) + BigInt(stringy(l2))).toString();
  let output = null;

  for (i = 0; i < outval.length; i++) {
    let newVal = new ListNode(outval[i]);
    newVal.next = output;
    output = newVal;
  }
  return output;
};