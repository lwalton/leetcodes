/**
 * @param {number} num
 * @return {number}
 */
// const findComplement = function (n) {
//   return parseInt(n.toString(2).split("").map(x=>x==='0'?1:0).join(''),2);
// };


// Attempt 2 - This is what I originally wanted to do, never converting to string

function b(n){
    let r = [];
    while(n >= 1 ){
        r.unshift(Math.floor(n%2) === 0 ? 1 : 0)
        n = n/2;
    }
    return r.join('');
}
const findComplement = function (n) {
  return parseInt(b(n),2);
};

console.log(findComplement(5));
