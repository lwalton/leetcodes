//First attempt

/**
 * @param {string} J
 * @param {string} S
 * @return {number}
 */
// var numJewelsInStones = function (J, S) {
//   J = J.split("");
//   S = S.split("");
//   let map = new Map();
//   let counter = 0;
//   S.forEach((t) => {
//     if (map.has(t)) {
//       map.set(t, map.get(t) + 1);
//     } else {
//       map.set(t, 1);
//     }
//   });
//   J.forEach((h) => {
//     let a = map.get(h);
//     counter += a > 0 ? a : 0;
//   });
//   return counter;
// };

// Attempt 2 - 1 fewer loop, slightly faster
// const numJewelsInStones = function (J, S) {
//   J = J.split("");
//   S = S.split("");
//   let c = 0;
//   S.forEach((t) => {
//     if(J.includes(t)) c ++;
//   });
//   return c;
// };

// Attempt 3 - very javascript, slower than above
// const numJewelsInStones = (J, S) => {
//     return Array.prototype.reduce.call(S,(acc,stone)=> J.includes(stone) ? acc+1 : acc,0);
// };

// Attempt 4 - for should be faster than forEach, and yet it was slower...
const numJewelsInStones = (J, S) => {
    J = J.split("");
    let c = 0;
    for(let i = 0; i < S.length;i++){
        if(J.includes(S.charAt(i)))c++;
    }
    return c;
};

console.log(numJewelsInStones("aAz", "aaAAZZ"));
