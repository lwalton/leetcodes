/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} x
 * @param {number} y
 * @return {boolean}
 */

// var isCousins = function (root, x, y) {
//   //Cast root form TreeNode to array
//   const queue = [root];

//   while (queue.length) {
//     const size = queue.length;
//     let foundX = false;
//     let foundY = false;

//     // Iterate over entire array
//     for (let i = 0; i < size; i++) {
//       // As root is now just an array, we can just shift off the front and compare values
//       const node = root.shift();

//       if (node.left && node.right) {
//         if (
//           ((node.left.val === x && node.right.val === y) ||
//           (node.left.val === y && node.right.val) === x)
//         ) {
//           return false;
//         }
//         if(node.val===x) foundX = true;
//         if(node.val===y) foudnY = true;
//         if (node.left) queue.push(node.left);
//         if (node.right) queue.push(node.right);
//       }
//       if (foundX && foundY) return true;
//       if(foundX) return false;
//       if(foundY) return false;
//     }
//     return false;
//   }
// };
// I cheated to get this.
// It works simply, and I added a couple of minor optimisations

const isCousins = function (r, x, y) {
  const queue = [r];
  let g, h;
  while (queue.length) {
    g = false;
    h = false;
    const s = queue.length;
    for (let i = 0; i < s; i++) {
      const n = queue.shift();
      if (n.left && n.right) {
        if (
          (n.left.val === x && n.right.val === y) ||
          (n.left.val === y && n.right.val === x)
        )
          return false;
      }
      if (n.val === x) g = true;
      if (n.val === y) h = true;
      if (n.left) queue.push(n.left);
      if (n.right) queue.push(n.right);
    }
    if (g && h) return true;
    if (g||h) return false;
  }
  return false;
};
