// Attempt 1: Map!
/**
 * @param {number[]} nums
 * @return {number}
 */
// const majorityElement = function (n) {
//   if (n.length < 2) return n[0];
//   const m = new Map();
//   for (let i = 0; i < n.length; i++) {
//     const x = m.get(n[i]);
//     if (x) {
//       if (x + 1 > n.length / 2) {
//         return n[i];
//       }
//       m.set(n[i], x + 1);
//     } else {
//       m.set(n[i], 1);
//     }
//   }
// };

// Attempt 2: Sorting (from solution) SLOWER!
// const majorityElement = function (n) {
//   if (n.length < 2) return n[0];
//   n.sort();
//   return n[Math.trunc(n.length / 2)];
// };

// Attempt 3: Boyer-Moore Voting
// This is counting up to find if a candiate has enough votes
// I dont entirely get it, but it seems smart
const majorityElement = (nums) => {
  let count = 0;
  let candidate;

  for (let i = 0; i < nums.length; i++) {
    if (count === 0) {
      candidate = nums[i];
    }
    count += nums[i] === candidate ? 1 : -1;
  }
  return candidate;
};

console.log(majorityElement([3, 2, 3]));
