/**
 * @param {number[][]} coordinates
 * @return {boolean}
 */
// ATtempt 1 - kind of copied, although this is what I was trying to do
// var checkStraightLine = function (coordinates) {
//   if (coordinates.length > 2) {
//     const diffx = coordinates[1][0] - coordinates[0][0];
//     const diffy = coordinates[1][1] - coordinates[0][1];

//     for (let i = 2; i < coordinates.length; i++) {
//       if (
//         diffx * (coordinates[i][1] - coordinates[0][1]) !==
//         diffy * (coordinates[i][0] - coordinates[0][0])
//       ) {
//         return false;
//       }
//     }
//   }
//   return true;
// };
// This shold be faster? I guesss not.
const checkStraightLine = function (c) {
  if (c.length > 3) {
    const s = (c[1][1] - c[0][1]) / (c[1][0] - c[0][0]);
    for (var i = 2; i < c.length; i++) {
      if (s != (c[i][1] - c[i - 1][1]) / (c[i][0] - c[i - 1][0])) {
        return false;
      }
    }
  }
  return true;
};

console.log(
  checkStraightLine([
    [1, 2],
    [2, 3],
    [3, 4],
    [4, 5],
    [5, 6],
    [6, 7],
  ])
);
