//First attempt
/**
 * @param {string} ransomNote
 * @param {string} magazine
 * @return {boolean}
 */
// var canConstruct = function (ransomNote, magazine) {
//   ransomNote = ransomNote.split("");
//   magazine = magazine.split("");
//   for (let i = 0; i < ransomNote.length; i++) {
//     const x = magazine.findIndex((x) => x === ransomNote[i]);
//     if (x > -1) {
//       magazine.splice(x, 1);
//     } else {
//       return false;
//     }
//   }
//   return true;
// };

// Second attempt - Trying a Map.
// Quicker, not quickest
const canConstruct = function (r, m) {
    if (r.length != 0 && m.length == 0) return false;
    if (r.length > m.length) return false;
    const a = new Map();
    for (let i = 0; i < m.length; i++) {
      const u = m.charAt(i);
      const x = a.has(u) ? a.get(u) : 0;
      a.set(u, x + 1);
    }
    for (let i = 0; i < r.length; i++) {
      const u = r.charAt(i);
      const x = a.has(u) ? a.get(u) : -1;
      if (x <= 0) return false;
      a.set(u, x - 1);
    }
    return true;
  };

// Attempt 3 - stolen form the internet, makes sense to me though
// I would not use char code, but that does make sense that it would take up less space
// Using the unicode char value takes: 52ms and 37.1mb

// var canConstruct = function (ransomNote, magazine) {
//   if (ransomNote.length != 0 && magazine.length == 0) return false;
//   if (ransomNote.length > magazine.length) return false;
//   var a = new Array(27);
//   a.fill(0, 0, 27);
//   for (var i = 0; i < magazine.length; i++) {
//     var unicode = magazine.charCodeAt(i) - 97;
//     a[unicode] = a[unicode] + 1;
//   }

//   for (var i = 0; i < ransomNote.length; i++) {
//     var unicode = ransomNote.charCodeAt(i) - 97;
//     a[unicode] = a[unicode] - 1;
//     if (a[unicode] < 0) return false;
//   }
//   return true;
// };


console.log(canConstruct("aa", "ab"));
