// Just the one attempt today, this one did so well that it seems like a waste of time to try again
// if I had tried again, I was thinking of using a Map rahter than an array, although I can't see that being better
// but it would be more readable as the -97 is like a magic number at the moment.
/**
 * @param {string} s
 * @return {number}
 */
const firstUniqChar = function (s) {
  const a = new Array(26).fill(0);
  const o = 97;
  for (let i = 0; i < s.length; i++) {
    a[s.charCodeAt(i) - o]++;
  }
  let r = 0;
  while (r < s.length) {
    if (a[s.charCodeAt(r) - 97] === 1) {
      return r;
    }
    r++;
  }
  return -1;
};

console.log(firstUniqChar("abcdefg"));
