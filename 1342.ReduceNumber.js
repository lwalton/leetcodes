const numberOfSteps = (num)=>{
    let x = 0;
    while(num > 0){
        num = (num % 2 === 0 ? num / 2 : num -1) ;
        x++;
    }
    return x;
}

console.log(numberOfSteps(123456789))