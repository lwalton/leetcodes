//First attempt - dont convert to string. compare each number individually

// const isPalindrome = x => {
//   if (x < 0) return false;

//   let arr = [];
//   while (x > 0) {
//     arr.unshift(x % 10);
//     x = (x / 10) | 0;
//   }

//   while (arr.length > 1) {
//     if (arr[0] === arr[arr.length - 1]) {
//       arr.splice(0, 1);
//       arr.splice(arr.length - 1, 1);
//     } else {
//       return false;
//     }
//   }
//   return true;
// };

//second attempt- convert to string
// const isPalindrome = x =>{
//     return x.toString() === x.toString().split("").reverse().join("");
// }

//third attempt - compare first and reversed second half

const isPalindrome = x => {
  if (x < 0) return false
  const arr = []
  while (x > 0) {
    arr.unshift(x % 10)
    x = (x / 10) | 0
  }
  const a = arr.slice(0, Math.trunc(arr.length / 2))
  const b = arr.slice(arr.length - Math.trunc(arr.length / 2), arr.length).reverse();
  return a.length === b.length && a.every((el, ix) => el === b[ix])
}

console.log(isPalindrome(112211));
