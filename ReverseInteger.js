/**
 * @param {number} x
 * @return {number}
 */
const reverse = function(x) {
  const str = x.toString();
  const out = parseInt(str.split("").reverse().join(""));
  if (out > Math.pow(2, 31) - 1 ||out < (-Math.pow(2, 31)) ) {
    return 0;
  }

  return (str.charAt(0) !== "-") ? out : -out;
};

console.log(reverse(123));
