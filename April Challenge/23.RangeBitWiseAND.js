/**
 * @param {number} m
 * @param {number} n
 * @return {number}
 */
// First attempt - too slow
// const rangeBitwiseAnd = function (m, n) {
//   let c = m;
//   m++;
//   for (m; m <= n; m++) {
//     c = c & m;
//   }
//   return c;
// };

// Second attempt - we know that as we increment, the numbers increase from right to left
// This can be leveraged as we know that it is only the digits for which both m,n are on the same level that matter
// Therefore, we right shift until both m,n are the same. From this, we know all the values that will be a 1 from the AND
// const rangeBitwiseAnd = function (m, n) {
//   let c = 0;
//   while(m!=n){
//       m>>=1;
//       n>>=1;
//       c++;
//   }
//   return m<<c;
// };

//Third attempt - Subtract the 2's compliment (-n) AND n from n.
// I am not sure why this works.
const rangeBitwiseAnd = (m, n) => {
  while (m < n) n -= n & -n;
  return n;
};
console.log(rangeBitwiseAnd(5, 7));
