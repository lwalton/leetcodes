const LRUCache = function (c) {
  this.capcatiy = c;
  this.map = new Map();
};
LRUCache.prototype.get = function (k) {
  return this.map.has(k) ? this.put(k, this.map.get(k)) : -1;
};
LRUCache.prototype.put = function (k, v) {
  if (this.map.has(k)) this.map.delete(k);
  this.map.set(k, v);
  if (this.map.size > this.capcatiy) {
    this.map.delete(this.map.keys().next().value);
  }
  return v;
};