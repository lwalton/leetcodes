/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number[]} arr
 * @return {boolean}
 */

var isValidSequence = function (root, arr) {
  if (arr) {
    if (arr.length === 1) {
      if (root.val === arr[0] && root.left === null && root.right === null) {
        return true;
      }
      return false;
    }
    if (root.val === arr[0]) {
      const arr2 = arr.slice(1, arr.length);
      let left = false,
        right = false;
      if (root.left) {
        left = isValidSequence(root.left, arr2);
      }
      if (root.right) {
        right = isValidSequence(root.right, arr2);
      }
      return left || right;
    }
    return false;
  }
  return true;
};

const isValidSequence = function (n, a) {
  if (a) {
    if (n.val === a[0]) {
      if (a.length === 1 && n.left === null && n.right === null) {
        return true;
      }
      const b = a.slice(1, a.length);
      let l = false,r = false;
      if (n.left) {
        l = isValidSequence(n.left, b);
      }
      if (n.right) {
        r = isValidSequence(n.right, b);
      }
      return l || r;
    }
    return false;
  }
  return true;
};