const countElements = function (a) {
  let c = 0, s = 1;
  a = a.sort((a, b) => a - b);
  for (let i = 0; i < a.length - 1; i++) {
    if (a[i] + 1 === a[i + 1]) {
      c += s;
      s = 1;
    } else if (a[i] === a[i + 1]) {
      s++;
    } else {
      s = 1;
    }
  }
  return c;
};

console.log(countElements([6, 3, 11, 6, 11, 1, 11, 4, 7, 6, 13, 4, 1]));