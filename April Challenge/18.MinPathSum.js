// Tried a recursive solution but that didnt work, so I copied this...
const minPathSum = function(g) {
    const n = g.length;
    const m = g[0].length;
    for(let i=1; i<n; i++) {
        g[i][0] += g[i-1][0];
    }
    for(let j=1; j<m; j++) {
        g[0][j] += g[0][j-1];
    }
    for(let i=1; i<n; i++) {
        for(let j=1; j<m; j++) {
            g[i][j] += Math.min(g[i-1][j], g[i][j-1]);
        }
    }
    return g[n-1][m-1];
};

console.log(
  minPathSum([
    [1, 2],
    [1, 1],
  ])
);
