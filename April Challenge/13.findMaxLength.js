// My solutino is too slow due to being brute force
// function isEqual(subArr) {
//   let counter = 0;
//   subArr.forEach(function (x) {
//     x === 0 ? counter++ : counter--;
//   });
//   return counter === 0;
// }

// var findMaxLength = function (nums) {
//   if (nums.length < 2) return 0;
//   if (nums.length === 2 && nums[0] !== nums[1]) return 2;
//   let i;
//   if (nums.length % 2 === 0) {
//     i = nums.length;
//   } else {
//     i = nums.length - 1;
//   }
//   for (i; i > 0; i -= 2) {
//     for (let j = 0; j <= nums.length - i; j++) {
//       if (isEqual(nums.slice(j, j + i))) {
//         return i;
//       }
//     }
//   }
//   return 0;
// };


//Stolen answer
const findMaxLength = function(n) {
    let h = {0:-1};
    let c = 0;
    let m = 0;
    for (let i=0;i<n.length;i++) {
        if (n[i] == 0) c--;
        else c++;

        if (h[c]!=null) m = Math.max(m, i - h[c]);
        else h[c] = i 
    }
    return m;
};

console.log(findMaxLength([0, 1,0]));
