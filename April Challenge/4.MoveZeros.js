const moveZeroes = n => {
  for(let i = n.length - 2; i > -1; i--){
      if(n[i] === 0) {
          n.splice(i,1);
          n.push(0);
      }
  }
};

moveZeroes([0, 0, 1]);