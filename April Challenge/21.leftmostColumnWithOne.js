/**
 * // This is the BinaryMatrix's API interface.
 * // You should not implement it, or speculate about its implementation
 * function BinaryMatrix() {
 *     @param {integer} x, y
 *     @return {integer}
 *     this.get = function(x, y) {
 *         ...
 *     };
 *
 *     @return {[integer, integer]}
 *     this.dimensions = function() {
 *         ...
 *     };
 * };
 */

/**
 * @param {BinaryMatrix} binaryMatrix
 * @return {number}
 */
const leftMostColumnWithOne = function (b) {
  const d = b.dimensions();
  if ((d[0] === 0) | (d[1] === 0)) {
    return -1;
  }
  let o = -1;
  let r = 0;
  let c = d[1] - 1;

  while (r < d[0] && c >= 0) {
    if (b.get(r, c) === 1) {
      o = c;
      c -= 1;
    } else {
      r += 1;
    }
  }
  return o;
};
