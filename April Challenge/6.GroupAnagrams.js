// const groupAnagrams = function (strs) {
//   let l = {};
//   for (let i = 0; i < strs.length; i++) {
//     const str = strs[i].split("").sort();
//     if (l[str]) {
//       l[str] .push(strs[i])
//     } else {
//       l[str] = [strs[i]];
//     }
//   }
//   return Object.values(l);
// };

// Minor improvements
// const groupAnagrams = function (strs) {
//   let h = {};
//   strs.forEach((s) => {
//     const l = s.split("").sort();
//     h[l] ? h[l].push(s) : (h[l] = [s]);
//   });
//   const keys = Object.keys(h);

//   const values = keys.map(function (v) {
//     return h[v];
//   });
//   return values
// };

var groupAnagrams = function(strs) {
    let res = new Map();
    strs.map(value=>{
        let str =[... value].sort().join('');
        if (res.has(str)){
            let arr = res.get(str)
            arr.push(value);
            res.set(str, arr);
        }else res.set(str,[value]);
    });
    return [...res.values()];
};

console.log(groupAnagrams(["eat", "tea", "tan", "ate", "nat", "bat"]));
