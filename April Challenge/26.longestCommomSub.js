/**
 * @param {string} text1
 * @param {string} text2
 * @return {number}
 */

// Attempted to brute force this, but I realised that the worse case would be far too slow

// I couldnt think of a better method, but copied this
// Works by mapping a 2d array to calculate which characters are in both strigns

var longestCommonSubsequence = function (text1, text2) {
  //   const dp = [];

  // How do you set up a 2d array in JS, and why does this work?
  const dp = [...Array(text1.length + 1)].map((e) => Array(text2.length + 1));

  // Loop over outer array
  for (let i = 0; i <= text1.length; i++) {
    // Loop over inner array
    for (let j = 0; j <= text2.length; j++) {
      // For the 0th row and column, the values are 0
      if (i === 0 || j === 0) {
        dp[i][j] = 0;
      } else if (text1.charAt(i - 1) === text2.charAt(j - 1)) {
    //   } else if (text1.charCodeAt(i - 1) === text2.charCodeAt(j - 1)) {
        dp[i][j] = dp[i - 1][j - 1] + 1;
        // accumulates values down the grid
      } else {
        dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
      }
    }
  }
  return dp[text1.length][text2.length];
};

console.log(longestCommonSubsequence("abcde", "ace"));
