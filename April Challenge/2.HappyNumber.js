const isHappy = n => {
  let s = [];
  while (n > 1 && !s.includes(n)) {
    s.push(n);
    let add = 0;
    while (n > 0) {
      add += Math.pow(n % 10, 2);
      n = parseInt(n / 10);
    }
    n = add;
  }
  return n === 1;
};

console.log(isHappy(19));
