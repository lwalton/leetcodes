/**
 * @param {number[]} nums / a for array
 * @param {number} k
 * @return {number}
 */

// My solution is brute force because I am lazy.
// const subarraySum = function (a, k) {
//   const l = a.length;
//   let c = 0;
//   let r = 0;
//   for (let i = 0; i < l; i++) {
//     for (let j = i; j < l; j++) {
//       c += a[j];
//       if (c === k) r++;
//     }
//     c = 0;
//   }
//   return r;
// };

// Second try: Cumulative sum - not faster, but slower due to more loops
// const subarraySum = function (nums, k) {
//   let count = 0,
//     sum = [0];
//   const l = nums.length;
//   for (let i = 1; i < l + 1; i++) {
//     sum[i] = sum[i - 1] + nums[i - 1];
//   }
//   for (let start = 0; start < l; start++) {
//     for (let end = start + 1; end <= l; end++) {
//       if (sum[end] - sum[start] === k) count++;
//     }
//   }
//   return count;
// };

//Third try: Hashmaps:
const subarraySum = function (n, k) {
  let h = new Map(), c = 0, s = 0;
  h.set(0, 1);
  for (let i = 0; i < n.length; i++) {
    s += n[i];
    if (h.has(s - k)) c += h.get(s - k);
    if (h.has(s)) {
      h.set(s, h.get(s) + 1);
    } else {
      h.set(s, 1);
    }
  }
  return c;
};
console.log(subarraySum([1, 2, 3], 3));
