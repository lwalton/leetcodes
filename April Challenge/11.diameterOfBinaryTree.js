/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
const diameterOfBinaryTree = function (r) {
  const depth = function (n) {
    if (n === null) return 0;
    const l = depth(n.left);
    const r = depth(n.right);
    a = Math.max(a, l + r + 1);
    return Math.max(l, r) + 1;
  };
  let a = 1;
  depth(r);
  return a - 1;
};
