const lastStoneWeight = function (s) {
  while (s.length > 1) {
    s = s.sort((a, b) => {return b - a;});
    if (s[0] === s[1]) {
      if (s.length === 2) {
        s.pop();
        s[0] = 0;
      } else {
        s.shift();
        s.shift();
      }
    } else {
      s[1] = s[0] - s[1];
      s.shift();
    }
  }
  return s[0];
};

console.log(lastStoneWeight([2, 7, 4, 1, 8, 1]));
lastStoneWeight([2, 7, 4, 1, 8, 1]);
