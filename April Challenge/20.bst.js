//  Definition for a binary tree node.
function TreeNode(val) {
  this.val = val;
  this.left = this.right = null;
}

/**
 * @param {number[]} preorder
 * @return {TreeNode}
 */
const bstFromPreorder = function (preorder) {
  if (!preorder.length) return null;
  let root = new TreeNode(preorder.shift());
  let smaller = [];
  while (preorder.length && preorder[0] < root.val) {
    smaller.push(preorder.shift());
  }
  root.left = bstFromPreorder(smaller);
  root.right = bstFromPreorder(preorder);
  return root;
};

console.log(bstFromPreorder([8, 5, 1, 7, 10, 12]));
