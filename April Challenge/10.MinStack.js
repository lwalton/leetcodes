const MinStack = function () {
  this.a = [];
  this.m = null;
};
MinStack.prototype.push = function (x) {
  this.a.push(x);
  if (x < this.m || this.m === null) {
    this.m = x;
  }
};
MinStack.prototype.pop = function () {
  const popped = this.a.pop();
  if (popped === this.m) {
    this.m = Math.min(...this.a);
  }
};
MinStack.prototype.top = function () {
  return this.a[this.a.length - 1];
};
MinStack.prototype.getMin = function () {
  return this.m;
};