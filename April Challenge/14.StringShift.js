/**
 * @param {string} s
 * @param {number[][]} shift
 * @return {string}
 */
// const stringShift = function (s, a) {
//   s = s.split("");
//   a.forEach((x) => {
//     if (x[0] === 0) {
//       const t = s.splice(0, x[1]);
//       s = [...s, ...t];
//     } else {
//       const t = s.splice(s.length - x[1], s.length);
//       s = [...t, ...s];
//     }
//   });
//   return s.join("");
// };
const stringShift = function (s, a) {
  s = s.split("");
  for(let i = 0; i < a.length; i++){
    if (a[i][0] === 0) {
        const t = s.splice(0, a[i][1]);
        s = [...s, ...t];
      } else {
        const t = s.splice(s.length - a[i][1], s.length);
        s = [...t, ...s];
      }
  }
  return s.join("");
};

console.log(
  stringShift("abcdefg", [
    [1, 1],
    [1, 1],
    [0, 2],
    [1, 3],
  ])
);
