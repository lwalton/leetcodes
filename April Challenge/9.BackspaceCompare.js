
const d = (T) => {
  T = T.split("");
  let f = T.indexOf("#");
  while (f > -1) {
      if (f === 0) {
          T.splice(f, 1);
        } else {
            T.splice(f - 1, 2);
        }
        f = T.indexOf("#");
    }
    return T.toString()
};

/**
 * @param {string} S
 * @param {string} T
 * @return {boolean}
 */
const backspaceCompare = function (S, T) {
  return d(S) === d(T);
};

console.log(backspaceCompare("a#c", "b"));