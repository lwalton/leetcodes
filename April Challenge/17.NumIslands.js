/**
 * @param {character[][]} grid
 * @return {number}
 */
const f = function (i, j, g) {
  if (
    i >= 0 &&
    j >= 0 &&
    i < g.length &&
    j < g[i].length &&
    g[i][j] == 1
  ) {
    g[i][j] = 0;
    f(i, j - 1, g);
    f(i, j + 1, g);
    f(i + 1, j, g);
    f(i - 1, j, g);
  }
};

const numIslands = function (g) {
  let count = 0;
  for (let i = 0; i < g.length; i++) {
    for (let j = 0; j < g[i].length; j++) {
      if (g[i][j] == "1") {
        count++;
        f(i, j, g);
      }
    }
  }
  return count;
};

console.log(
  numIslands([
    ["1", "1", "1", "1", "0"],
    ["1", "1", "0", "1", "0"],
    ["1", "1", "0", "0", "0"],
    ["0", "0", "0", "0", "0"],
  ])
);
