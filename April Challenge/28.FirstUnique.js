// At least I did this one myself. It is amazingly slow. Crazy Slow. Embarrassingly slow.

class FirstUnique {
  constructor(arr) {
    this.arr = arr;
    this.map = new Map();
    this.val = -1;

    for (let i = 0; i < this.arr.length; i++) {
      const found = this.map.get(this.arr[i]);
      if (found) {
        this.map.set(this.arr[i], found + 1);
      } else {
        this.map.set(this.arr[i], 1);
      }
    }
  }

  showFirstUnique() {
    for (let i = 0; i < this.arr.length; i++) {
      if (this.map.get(this.arr[i]) === 1) {
        this.val = this.arr[i];
        break;
      }
    }
    return this.val;
  }

  add(x) {
    this.arr.push(x);
    const found = this.map.get(x);
    if (found) {
      this.map.set(x, found + 1);
    } else {
      if (this.val === -1) {
        this.val = x;
      }
      this.map.set(x, 1);
    }
  }
}

let x = new FirstUnique([2, 3, 5]);

x.add(5);
x.add(2);
x.add(3);
console.log(x.showFirstUnique());
