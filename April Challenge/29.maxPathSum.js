/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */
const maxPathSum = function (r) {
  let m = -Infinity;

  const f = (n) => {
    if (!n) {
      return -Infinity;
    }

    let l = f(n.left);
    let r = f(n.right);

    let b = Math.max(
      n.val + l,
      n.val + r,
      n.val,
      n.val + l + r
    );
    m = Math.max(m, b);

    return n.val + Math.max(l, r, 0);
  };

  f(r);
  return m;
};
// var maxPathSum = function (root) {
//   let max = -Infinity;

//   const dfs = (node) => {
//     if (!node) {
//       return -Infinity;
//     }

//     let left = dfs(node.left);
//     let right = dfs(node.right);

//     let currentBest = Math.max(
//       node.val + left,
//       node.val + right,
//       node.val,
//       node.val + left + right
//     );
//     max = Math.max(max, currentBest);

//     return node.val + Math.max(left, right, 0);
//   };

//   dfs(root);
//   return max;
// };
