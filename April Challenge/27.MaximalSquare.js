// 27 days, and I finally forgot a day. Shocked that I made it that far.
/**
 * @param {character[][]} matrix
 * @return {number}
 */
const maximalSquare = function (m) {
  if (!m.length || !m[0].length) return 0;
  let d = m.slice(),
    q = Math.max(...m[0]);
  for (let i = 0; i < m.length; i++) {
    q = Math.max(m[i][0], q);
  }
  for (let i = 1; i < m.length; i++) {
    for (let j = 1; j < m[0].length; j++) {
      if (m[i][j] == "0") continue;
      d[i][j] = Math.min(d[i - 1][j], d[i][j - 1], d[i - 1][j - 1]) + 1;
      if (d[i][j] > q) q = d[i][j];
    }
  }
  return q ** 2;
};
// var maximalSquare = function (matrix) {
//   if (!matrix.length || !matrix[0].length) return 0;
//   let dp = matrix.slice(),
//     max = Math.max(...matrix[0]);
//   for (let i = 0; i < matrix.length; i++) {
//     max = Math.max(matrix[i][0], max);
//   }
//   for (let i = 1; i < matrix.length; i++) {
//     for (let j = 1; j < matrix[0].length; j++) {
//       if (matrix[i][j] == "0") continue;
//       dp[i][j] = Math.min(dp[i - 1][j], dp[i][j - 1], dp[i - 1][j - 1]) + 1;
//       if (dp[i][j] > max) max = dp[i][j];
//     }
//   }
//   return max ** 2;
// };
