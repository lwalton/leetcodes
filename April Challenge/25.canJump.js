/**
 * @param {number[]} nums
 * @return {boolean}
 */

// My attempt - certainly cheating...
// const canJump = function (n) {
//   if (n.length > 1) {
//     let f = n.findIndex((x) => x === 0);
//     while (f > -1 && f < n.length - 1) {
//       if (n[f - 1] > 1) {
//         n[f] = 1;
//       } else if (n[f - 2] > 2) {
//         n[f] = 1;
//       } else if (n[f - 3] > 3) {
//         n[f] = 1;
//       } else if (n[f - 4] > 4) {
//         n[f] = 1;
//       } else {
//         return false;
//       }
//       f = n.findIndex((x) => x === 0);
//     }
//   }
//   return true;
// };

// Attempt 2 -  Backtracking. This literally loops over all the numbers working out if it works.
// Optimizations made iterating from the end to the front of the list
// I also check if the arrary doesn't include 0, although I'm not sure if this actually helps.
// Too slow.
// const checkFromPos = (nums, pos) => {
//   if (pos === nums.length - 1 || !nums.includes(0)) {
//     return true;
//   }
//   const furthest = Math.min(pos + nums[pos], nums.length - 1);
//   //   for (let c = p + 1; c <= t; c++) {
//   for (let c = furthest; c > pos; c--) {
//     if (checkFromPos(nums, c)) {
//       return true;
//     }
//   }
//   return false;
// };
// const canJump = function (n) {
//   return checkFromPos(n, 0);
// };

// Third attempt - Dynamic programming using memorization
// Memo array contains 0 or 1 to symbolise good or unknown
// This doesnt use recursion as it goes backwards, which is a gain, but the gain is furthered in the next implementatioon

// const canJump = function (n) {
//   // 0 === BAD
//   // 1 === UNKNOWN
//   // 2 === GOOD
//   let memo = [];
//   n.forEach((x, i) => (memo[i] = 0));
//   memo[memo.length - 1] = 2;
//   for (let i = n.length - 2; i >= 0; i--) {
//     const furthestJump = Math.min(i + n[i], n.length - 1);
//     for (let j = i + 1; j <= furthestJump; j++) {
//       if (memo[j] === 2) {
//         memo[i] = 2;
//         break;
//       }
//     }
//   }
//   return memo[0] === 2;
// };

// Attempt 4 - Greedy
// Going backwards, it only needs a single iteration from the end to the beginning,
// the first value can either be good or bad, based on the cumulative results of all the others
const canJump = function (n) {
  let lastPos = n.length - 1;
  for (let i = n.length - 1; i >= 0; i--) {
    if (i + n[i] >= lastPos) {
      lastPos = i;
    }
  }
  return lastPos === 0;
};

//Despite all these attempts, the fastest algorithm performs exactly the same as when I cheated...
console.log(canJump([2, 3, 1, 1, 4]));
