/**
 * @param {string} s
 * @return {boolean}
 */

 // I had an idea to do it like this, but couldnt work it out.
 // This is the solution. I was close ish
const checkValidString = function (s) {
  let l = 0, h = 0;
  for (const c of s.split("")) {
    l += c === "(" ? 1 : -1;
    h += c !== ")" ? 1 : -1;
    if (h < 0) break;
    l = Math.max(l, 0);
  }
  return l === 0;
};

console.log(checkValidString("()"));
