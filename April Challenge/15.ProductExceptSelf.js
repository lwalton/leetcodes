// First approach did not finish in time...
// const productExceptSelf = function (n) {
//   return n.map((p, d) => {
//     let m = 1;
//     n.forEach((x, i) => {
//       if (i !== d) {
//         m *= x;
//       }
//     });
//     return m;
//   });
// };

// Still too slow
// const r = (a, v) => a * v;
// const productExceptSelf = function (n) {
//     let left = [], right = n, out = [], x;
//     while(right.length){
//         x = right.shift();
//         out.push(right.reduce(r, left.reduce(r, 1)) );
//         left.push(x);
//     }
//     return out;
// }

//Stolen answer
const productExceptSelf = function (n) {
  let p = 1;
  let a = [];
  let s;
  for (let j = 0; j < n.length; j++) {
    s = n[j];
    n[j] = 1;
    for (let v of n) {
      p *= v;
    }
    a.push(p);
    p = 1;
    n[j] = s;
  }
  return a;
};

console.log(productExceptSelf([1, 2, 3, 4]));
