const bubbleSort = inputArr => {
  const len = inputArr.length;
  for (i = 0; i < len; i++) {
    for (j = 0; j < len -1; j++) {
      if (inputArr[j] === 0) {
        let tmp = inputArr[j];
        inputArr[j] = inputArr[j + 1];
        inputArr[j + 1] = tmp;
      }
    }
  }
  return inputArr;
};

console.log(bubbleSort([0, 1, 0, 3, 12]));
