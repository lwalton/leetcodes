const subtractProductAndSum = n => {
  let s = 0,
    m = 1;
  while (n) {
    m *= n % 10;
    s += n % 10;
    n = Math.floor(n / 10);
  }
  return m - s;
};

console.log(subtractProductAndSum(4421));