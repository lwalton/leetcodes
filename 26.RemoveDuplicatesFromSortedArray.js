const removeDuplicates = nums => {
  let i = 0;
  while (i < nums.length) {
    if (nums[i] === nums[i + 1]) {
      let counter = 1;
      while (nums[i] === nums[i + counter + 1]) {
        counter++;
      }
      nums.splice(i + 1, counter);
    }
    i++;
  }
  return i;
};
console.log(
  removeDuplicates([1, 1, 1, 1, 1, 2, 2, 2, 3, 4, 5, 6, 7, 12, 12, 12, 22])
);
