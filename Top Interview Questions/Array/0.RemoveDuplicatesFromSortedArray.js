const removeDuplicates = (nums) => {
  let i = 0;
  while (i < nums.length) {
    if (nums[i] === nums[i + 1]) {
      let counter = 1;
      while (nums[i] === nums[i + counter + 1]) {
        counter++;
      }
      nums.splice(i + 1, counter);
    }
    i++;
  }
  return nums.length;
};

console.log(removeDuplicates([0, 0, 1, 1, 1, 2, 2, 3, 3, 4]));
