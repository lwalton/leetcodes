// const twoSum = (nums, target) => {
//   for (let i = 0; i < nums.length - 1; i++) {
//     for (let j = i + 1; j < nums.length; j++) {
//       if (nums[i] + nums[j] === target) {
//         return [i, j];
//       }
//     }
//   }
// };

const twoSum = (n, t) => {
  let N = {};
  n.map((a, b) => (N[a] = b));
  for(let i = 0; i < n.length; i++){
    let m = t-n[i];
    if(N[m] && N[m] !==i )return([i,N[m]])
  }
};

console.log(twoSum([1, 2, 3], 3));
