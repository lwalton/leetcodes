const largestNumber = nums => {
  nums.sort(function(x, y) {
    const v1 = parseInt(x.toString() + y.toString());
    const v2 = parseInt(y.toString() + x.toString());
    if (v1 > v2) return -1;
    if (v2 < v1) return 1;
    return 0;
  });
  return nums.toString().replace(/,/g, "");
};

console.log(largestNumber( [3,30,34,5,9]));